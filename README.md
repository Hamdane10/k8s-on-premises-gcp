# Repository Overview
This repository contains Terraform files, Ansible playbooks, and Bash scripts designed to provision a Kubernetes cluster on Google Cloud Platform (GCP) using Infrastructure as a Service (IaaS) approach with Terraform, Ansible, and Kubeadm.

## Key Components:
- **Terraform**: For provisioning the underlying infrastructure on GCP.
- **Ansible Playbooks**: To configure the provisioned infrastructure and manage Kubernetes setup.
- **Kubeadm**: Utilized for bootstrapping the Kubernetes cluster.
- **Scripts**: Bash scripts for automation of the deployment and destruction processes.

## Scripts at the Root of the Project:
1. **deploy.sh**: 
   - **Purpose**: Automates the provisioning of infrastructure in Google Cloud and deploys a Kubernetes cluster.
   - **Features**: 
     - Uses Kubeadm for Kubernetes cluster setup with default configurations specified in `scripts/env.sh`.
     - Provisions a High-Availability (HA) cluster with 3 master nodes and 3 worker nodes.
     - Configures an HA proxy load balancer for managing external and internal traffic.
   - **Usage**: Execute this script to initiate the deployment process.

2. **destroy.sh**: 
   - **Purpose**: Destroys the infrastructure created by `deploy.sh`.
   - **State Management**: The Terraform state is securely stored in a Google Cloud Storage bucket to ensure consistency and manage infrastructure lifecycle.

## Prerequisites:
- Google Cloud Account and SDK
- Terraform installed
- Ansible installed

## Getting Started:
1. Clone the repository.
2. Configure your Google Cloud credentials.
3. Modify `scripts/env.sh` with your desired configurations.
4. Run `./deploy.sh` to start the provisioning and setup process.
5. Once the cluster is up and running, you can manage it using standard Kubernetes tools.
6. Run `./destroy.sh` to cleanly remove all the resources when needed.

## Additional Information:
- The current configuration is not optimized for cost reduction. Users should be aware that the default settings may incur higher cloud costs.
- In the future, a remote exec feature in Terraform will be added to improve the coordination between Terraform and Ansible for more efficient infrastructure management.